<?php 

//fonction de récupération de l'API des dino
function recover_dinos()
{
    $response = Requests::get('https://allosaurus.delahayeyourself.info/api/dinosaurs/');
    
    return json_decode($response->body);
}

function recover_one_dino($name)
{
    $response = Requests::get('https://allosaurus.delahayeyourself.info/api/dinosaurs/'.$name);
    
    
    $dino = json_decode($response->body);

    return $dino;
}

use Michelf\Markdown;
// convertion du HTML en markdown
function renderHTMLFromMarkdown($string_markdown_formatted)
{
    return Markdown::defaultTransform($string_markdown_formatted);
}
function three_random_dino()
{
    $response = Requests::get('https://allosaurus.delahayeyourself.info/api/dinosaurs/');
    $decode = json_decode($response->body);
    $random_keys = array_rand($decode, 3); // On récupére trois clés aléatoires

    foreach($random_keys as $rand)
    {
        $dino[]=$decode[$rand];
    }
    return $dino;
}
