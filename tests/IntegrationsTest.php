<?php

class IntegrationsTest extends IntegrationTest
{
    //test sur la récupération des dinos
    public function test_index()
    {
        $response = $this->make_request("GET", "/");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsStringIgnoringCase("text/html", $response->getHeader('Content-Type')[0]);
        $body = $response->getBody()->getContents();
        foreach(recover_dinos() as $dino)
        {
            $this->assertStringContainsStringIgnoringCase($dino->avatar, $body);
            $this->assertStringContainsStringIgnoringCase($dino->name, $body);
        }
    }

    public function test_one_dino()
    {
        $response = $this->make_request("GET", "/dinosaur/brachiosaurus");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains("text/html", $response->getHeader('Content-Type')[0]);
        $body = $response->getBody()->getContents();
        $dino = recover_one_dino('brachiosaurus');
        $this->assertStringContainsStringIgnoringCase($dino->avatar, $body);
        $this->assertStringContainsStringIgnoringCase($dino->name, $body);
        $this->assertStringContainsStringIgnoringCase($dino->description, $body);
        $this->assertStringContainsStringIgnoringCase($dino->diet, $body);
        $this->assertStringContainsStringIgnoringCase($dino->height, $body);
        $this->assertStringContainsStringIgnoringCase($dino->length, $body);
        $this->assertStringContainsStringIgnoringCase($dino->weight, $body);
    }

    public function test_three_random_dino()
    {
        $response = $this->make_request("GET", "/dinosaur/brachiosaurus");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsStringIgnoringCase("text/html", $response->getHeader('Content-Type')[0]);
        $body = $response->getBody()->getContents();
        foreach(three_random_dino() as $dino)
        {
            $this->assertStringContainsStringIgnoringCase($dino->avatar, $body);
            $this->assertStringContainsStringIgnoringCase($dino->name, $body);
        }
    }
}


