<?php 
require "vendor/autoload.php";
$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');

$twigConfig = array
(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

//Integration de Twig dans Flight
Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) 
{
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
    
    //Mon filtre twig transformant le html en markdown
    $twig->addFilter(new Twig_Filter('markdown', function($string)
    {
        return renderHTMLFromMarkdown($string);
    }));

});

//utiliser render au lieu de view()->display()
Flight::map('render', function($template, $data=array())
{
    Flight::view()->display($template, $data);
});

//route vers la page home du site
Flight::route('/', function()
{
    $data = 
    [
        'dinos' => recover_dinos(),
    ];

    Flight::render('dinos.twig', $data);
});

//Route générique pour chaque dino
Flight::route('/dinosaur/@name', function($name)
{
    $data = 
    [
        'dinosaur' => recover_one_dino($name),
        'top' => three_random_dino()
    ];
    Flight::render('onedino.twig', $data);
});
Flight::start();